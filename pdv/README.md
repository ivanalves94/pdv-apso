# PdvAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.1.

## Prerequisites

### NodeJS

To install, go to nodejs.org, download and install version v6.11.2 LTS

#### NodeJS Dependencies

`npm install --save`

After installing the modules, go to the folder: `pdv-angular/node_modules/ng2-opd-popup/`
Remove the folder: `/node_modules`

### TypeScript

`npm install -g typescript`

### @angular/cli

`npm install -g @angular/cli`

## Start Project

To run the project, access the project directory and run the following command:
`npm start`

## Settings File

To enter configuration data, such as ip and port of the application server, you must:
Go to the folder: `pdv-angular/src/app/data`
Open the file: `settings.data.ts`
Edit the file:
`import {Settings} from '../models/settings';`
`export const settings: any[] =`
`[{`
   `"ip": "192.168.0.170",`
    `"port": "8080"`
`}];`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
