import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListItemsCartComponent} from './components/cart/list-itens/list-items-cart.component';
import {HomeComponent} from './components/home/home.component';
import {PaymentComponent} from './components/cart/payment/payment.component';
import {TypecardComponent} from './components/cart/typecard/typecard.component';
import {DetailMoneyComponent} from './components/cart/detail-money/detail-money.component';
import {SearchClientComponent} from './components/client/search-client/search-client.component';
import {NewClientComponent} from './components/client/new-client/new-client.component';
import {AdminComponent} from './components/admin/admin.component';
import {ListItemsClientComponent} from './components/client/list-items/list-items-client.component';
import {EditItemClientComponent} from './components/client/edit-item/edit-item-client.component';
import {ListItemsProductComponent} from './components/product/list-items/list-items-product.component';
import {NewProductComponent} from './components/product/new-product/new-product.component';
import {EditItemProductComponent} from './components/product/edit-item/edit-item-product.component';
import {NewEmployeeComponent} from './components/employee/new-employee/new-employee.component';
import {ListEmployeesComponent} from './components/employee/list-employees/list-employees.component';
import {LoginComponent} from './components/login/login.component';
import {EditEmployeeComponent} from './components/employee/edit-employee/edit-employee.component';
import { RegisterComponent } from './components/register/register.component';
import { SettingsComponent } from './components/settings/settings.component';
import { StatisticComponent } from './components/statistic/statistic.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'clients', component: ListItemsClientComponent},
  {path: 'search-client', component: SearchClientComponent},
  {path: 'new-client/:vender', component: NewClientComponent},
  {path: 'new-client', component: NewClientComponent},
  {path: 'edit-client/:id', component: EditItemClientComponent},
  {path: 'products', component: ListItemsProductComponent},
  {path: 'new-product', component: NewProductComponent},
  {path: 'edit-product/:id', component: EditItemProductComponent},
  {path: 'cart', component: ListItemsCartComponent},
  {path: 'payment', component: PaymentComponent},
  {path: 'typecard', component: TypecardComponent},
  {path: 'detail-money', component: DetailMoneyComponent},
  {path: 'employees', component: ListEmployeesComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'new-employee', component: NewEmployeeComponent},
  {path: 'edit-employee/:id', component: EditEmployeeComponent},
  {path: 'statistic', component: StatisticComponent},
  {path: 'settings', component: SettingsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
