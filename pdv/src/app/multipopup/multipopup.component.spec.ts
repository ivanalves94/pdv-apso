import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipopupComponent } from './multipopup.component';

describe('MultipopupComponent', () => {
  let component: MultipopupComponent;
  let fixture: ComponentFixture<MultipopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
