import {Injectable} from '@angular/core';
import {Client} from '../models/client';
import {clients} from '../data/clients.data';

@Injectable()
export class ClientService {

    clients: Client[];

    constructor() {
        this.clients = clients;
    }

    getClients(): Client[] {
        return this.clients;
    }

    getClientById(id: number): Client {
        return this.clients.filter(client => client.id === id).pop();
    }

    getClientByCpf(cpf: string): Client {
        return this.clients.filter(client => client.cpf === cpf).pop();
    }

    updateClient(client: Client) {
        const noUpdateClient = this.clients.filter(oldClient => oldClient.id === client.id).pop();
        noUpdateClient.name = client.name;
        noUpdateClient.telephone = client.telephone;
        noUpdateClient.cpf = client.cpf;
        noUpdateClient.address = client.address;

    }

    saveClient(client: Client): void {
        let id = this.clients.slice().pop().id;
        client.id = ++id;
        this.clients.push(client);
    }

    removeClient(client: Client): void {
        this.clients = this.clients.filter(oldClient => oldClient !== client);
    }

}
