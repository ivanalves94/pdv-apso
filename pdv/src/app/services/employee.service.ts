import { Injectable } from '@angular/core';
import { Employee } from '../models/employee';
import { employees } from '../data/employees.data';

@Injectable()
export class EmployeeService {

    employees: Employee[];

    constructor() {
      this.employees = employees;
    }

    getEmployees(): Employee[] {
      return this.employees;
    }

    getEmployeeById(id: number): Employee {
      return this.employees.filter(employee => employee.id === id).pop();
    }
    /*
    getClientByCpf(cpf: string): Employee {
      return this.clients.filter(client => client.cpf === cpf).pop();
    }*/

    updateEmployee(employee: Employee) {
      const noUpdateEmployee = this.employees.filter(oldEmployee => oldEmployee !== employee).pop();
      noUpdateEmployee.name = employee.name;
      noUpdateEmployee.email = employee.email;
      noUpdateEmployee.cpf = employee.cpf;
    }

    saveEmployee(employee: Employee): void {
      let id = this.employees.slice().pop().id;
      employee.id = ++id;
      this.employees.push(employee);
    }

    removeEmployee(employee: Employee): void {
      this.employees = this.employees.filter(oldEmployee => oldEmployee !== employee);
    }

}
