import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Response, RequestOptions } from '@angular/http';
import {Cart} from '../models/cart';
import {Settings} from '../models/settings';
import {settings} from '../data/settings.data';
import {Device} from '../models/device';
import { Observable } from 'rxjs';
import {DevicesService} from '../services/devices.service';
import {SettingsService} from '../services/settings.service';
import {XmlBuilder} from 'xmlbuilder';

@Injectable()
export class CommunicationService{

    private headers = new Headers({'Content-Type': 'application/json'});
    private options = new RequestOptions({headers: this.headers});

    settingsService: SettingsService;    
    device: Device;
    numeroSessaoList = [];
    numeroSessao: number;
    xml: XmlBuilder;

    constructor(private http: Http, private devicesService: DevicesService) {         
       this.devicesService = new DevicesService();
    }
    montarUrl(settings){
        for (let i = 0; i < settings.length; i++) {
            const ip = new String(settings[i].ip);
            const porta = new String(settings[i].port);
            const serverUrl = new String('http://' + ip + ':' + porta + '/');
            return serverUrl;
        }
    }
    
    descobertaDispositivo(){
        return this.http.get(this.montarUrl(settings) + 'mp4200/descobrir');
    }

    imprimir(texto: string) {
        this.devicesService.getDeviceStorage();
        const mac = localStorage.getItem('mac');
        const body = JSON.stringify({texto: texto, mac: mac});
        return this.http.post(this.montarUrl(settings) + 'mp4200/imprimir', body, this.options).subscribe((res) => console.log(res));
    }

    esperaImpressao(texto: string, tempo: number) {        
        setTimeout(() => {
            this.imprimir(texto);
        }, 4000 * tempo);
    }    

    finalizarCompra(cart: Cart) {
        this.enviarDadosVenda();        
        cart.cartItem.forEach((cartItem, index) => {
            console.log(index);            
            this.esperaImpressao(cartItem.product.name + ' -  R$ ' + cartItem.product.price.toString() + ' - ' + cartItem.quantity.toString() + ' itens - subtotal :  R$ ' + cartItem.total.toString(), index);
        });
        this.esperaImpressao('Total da Venda:' + cart.total.toString(), cart.cartItem.length);
    }
    /**
     * Dados da venda que serão enviados para o SAT no momento da finalização da venda
     * Equivale ao campo dadosVenda no XML que é enviado no enviarDadosVenda     *
     */
    //Deve retornar uma string com o xml dos dados da venda
    gerarDadosCompra(cart: Cart){
       let totalCompra = cart.total;
       cart.cartItem.forEach((cartItem, index) => {
           let qCom = cartItem.quantity;
           let vUnCom = cartItem.product.price;
           let cProd = cartItem.product.id;
           let xProd = cartItem.product.description;
           console.log("quatidade: "+qCom);
       })      
      /* let xml = this.xml.create('CFe')
       .ele('prod')
         .ele('cProd', {'xProd': 'git'}, 'git://github.com/oozcitak/xmlbuilder-js.git')
       .end({ pretty: true});
     
        console.log(xml);*/
       console.log("total compra: "+totalCompra);
    }

    /**
    * Exporta a função de EnviarDadosVenda do SAT
    *
    * numSessao - Inteiro, com 6 dígitos, contendo o número aleatório gerado pelo AC para controle da comunicação.
    * codAtivacao - String com no mínimo 8 e no máximo 32 caracteres, contendo a senha definida pelo contribuinte no software de ativação.
    * dados - String de tamanho livre porém não nulo, contendo os dados de venda gerados pelo aplicativo comercial e utilizados para compor o CF-e-SAT.
    */
    enviarDadosVenda(){
        this.gerarNumeroSessao();
        const body = JSON.stringify({"numeroSessao": 787879,"codigoDeAtivacao":"bilbao123","dadosVenda": "xml de venda"});
        return this.http.post(this.montarUrl(settings) + 'sat/dadosvenda', body, this.options).subscribe((res) => console.log(res));
    }

     /**
	* Exporta a função de CancelarUltimaVenda do SAT
	*
	* numSessao - Inteiro, com 6 dígitos, contendo o número aleatório gerado pelo AC para controle da comunicação.
	* codAtivacao - String com no mínimo 8 e no máximo 32 caracteres, contendo a senha definida pelo contribuinte no software de ativação.
	* chave - String com 47 caracteres, contendo a chave de acesso do CF-e-SAT a ser cancelado.
	* dados - String de tamanho livre porém não nulo, contendo os dados da venda gerados pelo aplicativo comercial e utilizados para compor o CF-e-SAT de cancelamento.
	*/
    cancelarUltimaVenda(){
        this.gerarNumeroSessao();
        const body = JSON.stringify({"numeroSessao":999999, "codigoDeAtivacao":"bilbao123","chave":"chave", "dadosCancelamento":"xml de cancelamento"});
        return this.http.post(this.montarUrl(settings) + 'sat/cancelarultimavenda', body, this.options).subscribe((res) => console.log(res));    
    }

    /**
     * Guardar apenas os últimos 100 números gerados
     * Verificar se existe algum número repetido, em caso afirmativo, gerar outro número
     */
    gerarNumeroSessao(){
        if (this.numeroSessaoList!=null) {
            if (this.numeroSessaoList.length<=100) {
                this.numeroSessao = Math.floor(Math.random() * 999999) + 1;                
               if (this.numeroSessaoList.filter(numeroSessao => numeroSessao === this.numeroSessao)[0] == null) {
                    this.numeroSessaoList.push(this.numeroSessao);
                    localStorage.setItem('numeroSessaoList',JSON.stringify(this.numeroSessaoList));
                } else {
                    this.numeroSessao = Math.floor(Math.random() * 999999) + 1;
                    this.numeroSessaoList.push(this.numeroSessao);
                    localStorage.setItem('numeroSessaoList',JSON.stringify(this.numeroSessaoList));
                }
            } else {
                this.numeroSessaoList = [];
                this.numeroSessao = Math.floor(Math.random() * 999999) + 1;
                this.numeroSessaoList.push(this.numeroSessao);
                localStorage.setItem('numeroSessaoList',JSON.stringify(this.numeroSessaoList));
            }            
        } else {
            this.numeroSessaoList = [];
            this.numeroSessao = Math.floor(Math.random() * 999999) + 1;
            this.numeroSessaoList.push(this.numeroSessao);
            localStorage.setItem('numeroSessaoList',JSON.stringify(this.numeroSessaoList));
        } 
        let lista = JSON.parse(localStorage.getItem('numeroSessaoList'));
        console.log(lista);       
        console.log(this.numeroSessaoList);        
        console.log(this.numeroSessao);
        return this.numeroSessao;
    }    

}