import {Injectable} from '@angular/core';

import {Product} from '../models/product';
import {products} from '../data/products.data';


@Injectable()
export class ProductService {

    products: Product[];

    constructor() {
        this.products = products;
    }

    getProducts(): Product[] {
        return this.products;
    }

    getProductById(id: number): Product {
        return this.products.filter(product => product.id === id).pop();
    }

    saveProduct(product: Product): void {
        let id = this.products.slice().pop().id;
        product.id = ++id;
        this.products.push(product);
    }

    updateProduct(product: Product) {
        const noUpdateProduct = this.products.filter(oldProduct => oldProduct.id === product.id).pop();
        noUpdateProduct.name = product.name;
        noUpdateProduct.description = product.description;
        noUpdateProduct.price = product.price;
    }

    removeProduct(product: Product): void {
        this.products = this.products.filter(oldProduct => oldProduct !== product);
    }

}
