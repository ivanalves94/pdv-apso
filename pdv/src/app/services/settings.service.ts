import {Injectable} from '@angular/core';
import {Settings} from '../models/settings';
import {settings} from '../data/settings.data';

@Injectable()
export class SettingsService {
    setting: Settings[];
       
    
        constructor() {           
           this.setting = settings;
        }
    
        getSetting(): Settings[] {
            return this.setting;
        }
    
        addSetting(settings: Settings[]): void {           
        }        
    
        removeSetting(setting: Settings): void {
           //TODO: Se mais de uma conf então implementar remoção
            this.initSettings();
        }
    
        existsSetting(setting: Settings[]): void {         
        }
    
        updateSettings() {
            this.initSettings();                    
            for (var index = 0; index < this.setting.length; index++) {                
                this.setting[index].ip = settings[index].ip;
                this.setting[index].port = settings[index].port;                
            }            
        }
    
        initSettings() {
            for (var index = 0; index < this.setting.length; index++) {
                this.setting[index].ip = '';
                this.setting[index].port = '';
            }            
        }
}