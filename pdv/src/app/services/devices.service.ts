import { Injectable } from '@angular/core';
import {Device} from '../models/device';
import {CommunicationService} from '../services/communication.service';
import {SettingsService} from '../services/settings.service';
import {Http, Headers} from '@angular/http';

@Injectable()
export class DevicesService {    
    
    devices = new Array<Device>(); 
    device: Device;

    constructor() {          
    } 
    
    setDevice(device): Device {
        this.device = new Device(device.id,device.ip,'8080',device.name,device.mac);                        
        console.log(this.device);
        return this.device;
    }

    getDevice(): Device {      
        return this.device;
    }

    getDevices(): Device[] {        
        return this.devices;
    }

    getDeviceById(id: string): Device {
        return this.devices.filter(device => device.id === id).pop();
    }

    getDeviceByIP(ip: string): Device {
        return this.devices.filter(device => device.ip === ip).pop();
    }

    storeDevice(device): Device {
        localStorage.setItem('id', device.id);
        localStorage.setItem('ip', device.ip);
        localStorage.setItem('port', device.port);
        localStorage.setItem('name', device.name);
        localStorage.setItem('mac', device.mac);
        this.device = new Device(device.id,device.ip,'8080',device.name,device.mac);
        console.log(this.device);
        return this.device;
    }

    getDeviceStorage(){       
        this.device = new Device(localStorage.getItem('id'),
        localStorage.getItem('ip'),
        localStorage.getItem('port'),
        localStorage.getItem('name'),
        localStorage.getItem('mac'));
        console.log(this.device);
    }
    
}
