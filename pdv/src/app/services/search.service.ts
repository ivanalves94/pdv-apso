import {Injectable} from '@angular/core';
import {Client} from '../models/client';
import {clients} from '../data/clients.data';

@Injectable()
export class SearchService {

    constructor() {

    }

    private searchCPF: string;
    setCPF(cpf: string){
        this.searchCPF = cpf;
    }

    getCPF(){
        return this.searchCPF;
    }

    private cpf_semCadastro: string;

    setCPF_NCadastrado(cpf: string){
        this.cpf_semCadastro = cpf;
    }

    getCPF_NCadastrado(){
        return this.cpf_semCadastro;
    }
}
