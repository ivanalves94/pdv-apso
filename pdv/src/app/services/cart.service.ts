import {Injectable} from '@angular/core';
import {Cart} from '../models/cart';
import {cart} from '../data/cart.data';
import {Product} from '../models/product';
import {CartItem} from '../models/cart-item';

@Injectable()
export class CartService {

    cart: Cart;

    constructor() {
        this.cart = cart;
    }

    getCart(): Cart {
        return this.cart;
    }

    addCartItem(cartItem: CartItem): void {
        const oldCartItem = this.existsProduct(cartItem.product);
        if (!oldCartItem) {
            this.cart.cartItem.push(cartItem);
        } else {
            oldCartItem.quantity += cartItem.quantity;
            oldCartItem.total += cartItem.total;
        }
        this.updateCartTotal();
    }

    removeCartItem(cartItem: CartItem): void {
        this.cart.cartItem = this.cart.cartItem.filter(oldCartItem => oldCartItem !== cartItem);
        this.updateCartTotal();
    }

    existsProduct(product: Product): CartItem {
        return this.cart.cartItem.filter(cartItem => cartItem.product === product).pop();
    }

    updateCartTotal() {
        let total = 0;
        this.cart.cartItem.forEach(item => total += item.total);
        this.cart.total = total;
    }

    cleanCart() {
        this.cart.total = 0;
        this.cart.cartItem = [];
    }
}
