import {Product} from '../models/product';

// produtos de teste
export const products = [
    new Product(
        1,
        'Arroz Urbano ',
        'Arroz Urbano - TIPO 1 - 1KG',
        3.499
    ),
    new Product(
        2,
        'Feijão Urbano',
        'Feijão Urbano - tipo 1 - 1kg',
        4.599
    ),
    new Product(
        3,
        'Leite Maranguape',
        'Leite Maranguape - tipo 1 - 1000 ml',
        3.99
    ),
    new Product(
        4,
        'Arroz Integral Urbano ',
        'Arroz Urbano - Integral - 1KG',
        5.45
    ),
    new Product(
        5,
        'Farinha ',
        'Farinha - 1KG',
        3.50
    ),
];
