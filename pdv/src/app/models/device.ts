export class Device {
    constructor(public id: string, public ip: string, public port: string, public name: string, public mac: string) {
    }
}