export class Client {
    constructor(public id: number, public name: string, public telephone: string, public cpf: string, public address: string) {
    }
}
