export enum Status {
    CompraFinalizada,
    CompraNaoFinalizada,
    PagamentoRealizado
}
