import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ItemCartComponent} from './components/cart/item/item-cart.component';
import {ListItemsCartComponent} from './components/cart/list-itens/list-items-cart.component';
import {ProductService} from './services/product.service';
import {SearchService} from './services/search.service';
import {CartService} from './services/cart.service';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './components/home/home.component';
import {PaymentComponent} from './components/cart/payment/payment.component';
import {TypecardComponent} from './components/cart/typecard/typecard.component';
import {DetailMoneyComponent} from './components/cart/detail-money/detail-money.component';
//import {CommunicationService} from './services/communication.service';
import {ClientService} from './services/client.service';
import {SearchClientComponent} from './components/client/search-client/search-client.component';
import {NewClientComponent} from './components/client/new-client/new-client.component';
import {AdminComponent} from './components/admin/admin.component';
import {ListItemsClientComponent} from './components/client/list-items/list-items-client.component';
import {ItemClientComponent} from './components/client/item/item-client.component';
import {EditItemClientComponent} from './components/client/edit-item/edit-item-client.component';
import {ListItemsProductComponent} from './components/product/list-items/list-items-product.component';
import {ItemProductComponent} from './components/product/item/item-product.component';
import {NewProductComponent} from './components/product/new-product/new-product.component';
import { EditItemProductComponent } from './components/product/edit-item/edit-item-product.component';
import { BackComponent } from './components/back/back.component';
import {NewEmployeeComponent} from './components/employee/new-employee/new-employee.component';
import { ListEmployeesComponent } from './components/employee/list-employees/list-employees.component';
import { ItemEmployeeComponent } from './components/employee/item-employee/item-employee.component';
import {EmployeeService} from './services/employee.service';
import { LoginComponent } from './components/login/login.component';
import {LoginService} from './services/login.service';
import { InformationComponent } from './components/cart/information/information.component';
import {PopupModule} from 'ng2-opd-popup';
import { MultipopupComponent } from './multipopup/multipopup.component';
import { ConfirmCartComponent } from './components/cart/confirm-cart/confirm-cart.component';

import { EditEmployeeComponent } from './components/employee/edit-employee/edit-employee.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { RegisterComponent } from './components/register/register.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { SettingsComponent } from './components/settings/settings.component';
import { BrMasker4Module } from 'brmasker4';
import { StatisticComponent } from './components/statistic/statistic.component';

var firebase = {
  apiKey: "AIzaSyB4uDSnzoJuqvq9Lfwazu2E-5L2HokyedA",
  authDomain: "pdv-apso.firebaseapp.com",
  databaseURL: "https://pdv-apso.firebaseio.com",
  projectId: "pdv-apso",
  storageBucket: "pdv-apso.appspot.com",
  messagingSenderId: "614708355754"
}


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ItemCartComponent,
    ListItemsCartComponent,
    HomeComponent,
    PaymentComponent,
    TypecardComponent,
    DetailMoneyComponent,
    SearchClientComponent,
    NewClientComponent,
    AdminComponent,
    ListItemsClientComponent,
    ItemClientComponent,
    EditItemClientComponent,
    ListItemsProductComponent,
    ItemProductComponent,
    NewProductComponent,
    EditItemProductComponent,
    BackComponent,
    NewEmployeeComponent,
    ListEmployeesComponent,
    ItemEmployeeComponent,
    InformationComponent,
    LoginComponent,
    MultipopupComponent,
    ConfirmCartComponent,
    EditEmployeeComponent,
    RegisterComponent,
    SettingsComponent,
    StatisticComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    BrMasker4Module,
    ReactiveFormsModule, 
    AngularFireModule.initializeApp(firebase),
    PopupModule.forRoot()
  ],
  providers: [
    SearchService,
    ProductService,
    CartService,
   // CommunicationService,
    ClientService,
    EmployeeService,
    LoginService,
    AngularFireAuth, 
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
