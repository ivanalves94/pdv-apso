import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {



  constructor(public afAuth: AngularFireAuth, private loginService: LoginService) {

  }

  id: any;
  email: any;
  nameUser: String = "Ivan Sousa";
  ngOnInit() {
    this.id = this.loginService.getIdUser();
    this.email = this.loginService.getEmailUser();

  }

}
