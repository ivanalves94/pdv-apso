import { Component, OnInit } from '@angular/core';
import {Employee} from '../../../models/employee';
import {Client} from '../../../models/client';
import {EmployeeService} from '../../../services/employee.service';
import {ClientService} from '../../../services/client.service';
import {SearchService} from '../../../services/search.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  employee: Employee;
  client: string;
  data: Date;

  constructor(public employees: EmployeeService, public clients: ClientService, public searchService: SearchService) {
  }

  ngOnInit() {
      this.employee = this.employees.getEmployeeById(1);
      const clientSearch = this.clients.getClientByCpf(this.searchService.getCPF());
      if (clientSearch) {
        this.client = this.clients.getClientByCpf(this.searchService.getCPF()).name;
      } else {
        this.client = this.searchService.getCPF();
      }
      this.data = new Date();
  }
}
