import {Component, Input, OnInit} from '@angular/core';
import {CartItem} from '../../../models/cart-item';
import {CartService} from '../../../services/cart.service';

@Component({
    selector: '[app-item-cart]',
    templateUrl: './item-cart.component.html',
    styleUrls: ['./item-cart.component.css']
})
export class ItemCartComponent implements OnInit {
    @Input() cartItem: CartItem;

    constructor(private cartService: CartService) {
    }

    removeCartItem() {
        this.cartService.removeCartItem(this.cartItem);
    }

    addProduct() {
        this.cartItem.total = this.cartItem.product.price * ++this.cartItem.quantity;
        this.cartService.updateCartTotal();
    }

    removeProduct() {
        this.cartItem.total = this.cartItem.product.price * --this.cartItem.quantity;
        if (this.cartItem.total === 0) {
            this.cartService.removeCartItem(this.cartItem);
        } else {
            this.cartService.updateCartTotal();
        }
    }

    ngOnInit() {
    }

}
