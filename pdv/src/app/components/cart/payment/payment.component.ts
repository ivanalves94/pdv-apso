import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CartService} from '../../../services/cart.service';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

    constructor(private router: Router, private cartService: CartService) {
    }

    // limpa o carrinho de compras
    cancelarCompra() {
        if (confirm('Deseja cancelar a compra?')) {
             this.cartService.cleanCart();
            this.router.navigate(['home']);
        }
    }


    ngOnInit() {
    }

}
