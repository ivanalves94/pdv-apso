import {Component, OnInit, ViewChild} from '@angular/core';
import {CartService} from '../../../services/cart.service';
import {Cart} from '../../../models/cart';
//import {CommunicationService} from '../../../services/communication.service';
import {Popup} from 'ng2-opd-popup';
import {Router} from '@angular/router';


@Component({
    selector: 'app-detail-money',
    templateUrl: './detail-money.component.html',
    styleUrls: ['./detail-money.component.css']
})
export class DetailMoneyComponent implements OnInit {
  @ViewChild('popupFinalizado') popupFinalizado: Popup;
  cart: Cart;

    constructor(private cartService: CartService,  private router: Router) {
    }

    // carrega o carrinho de compras ao iniciar o componente
    ngOnInit() {
        this.cart = this.cartService.getCart();
    }

    finalizarCompra() {
       // this.communicationService.gerarDadosCompra(this.cart);
      // this.communicationService.finalizarCompra(this.cart);
        this.popupFinalizado.options = {
          confirmBtnContent: 'Ok',
          cancleBtnContent: 'Cancelar',
          cancleBtnClass: 'btn btn-default',
          confirmBtnClass: 'btn btn-default',
          color: '#5cb85c',
          header: 'Sucesso!',
          widthProsentage: 60,
          animation: 'fadeInUp'
        };
        this.popupFinalizado.show(this.popupFinalizado.options);
    }
        Cancel(){
          /*this.popupEfetuarCompra.hide();*/
        }


        Voltar() {
          this.cartService.cleanCart();
          //this.router.navigate(['cart']);
          this.router.navigate(['search-client']);
        }
}
