import {Component, OnInit, ViewChild} from '@angular/core';
import {Cart} from '../../../models/cart';
import {CartService} from '../../../services/cart.service';
import {Product} from '../../../models/product';
import {CartItem} from '../../../models/cart-item';
import {ProductService} from '../../../services/product.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Popup} from 'ng2-opd-popup';

@Component({
    selector: 'app-list-items-cart',
    templateUrl: './list-items-cart.component.html',
    styleUrls: ['./list-items-cart.component.css']
})
export class ListItemsCartComponent implements OnInit {

    cart: Cart;

    productForm: FormGroup;
    @ViewChild('popupEfetuarCompra') popupEfetuarCompra: Popup;

    /*Lista que armazena todos os produtos disponíveis*/
    products: Product[];

    constructor(private cartService: CartService, private productService: ProductService, private router: Router, private fb: FormBuilder) {
        this.productForm = fb.group({
            'codigo': ['', Validators.required],
            'quantidade': ['', Validators.required]
        });
    }

    /**
     * Carrega o carrinho de compras e os produtos ao iniciar o componente 
     */ 
    ngOnInit() {
        console.log(this.cart);
        this.products = this.productService.getProducts();
        this.cart = this.cartService.getCart();
    }

    onSubmit() {
        const produto: Product = this.productService.getProductById(this.productForm.get('codigo').value);
        if ((produto) != undefined){
            const cartItem: CartItem = new CartItem(produto, this.productForm.get('quantidade').value, produto.price * this.productForm.get('quantidade').value);
            this.cartService.addCartItem(cartItem);
        } else {
            alert ("Código de produto inexistente!")
        }
    }

    /*Usa o alert para impressao dos dados contidos no pedido que é o vetor de cartItem[]*/
    efetuarPagamento() {
        this.popupEfetuarCompra.options = {
            confirmBtnContent: 'Confirmar',
            cancleBtnContent: 'Cancelar',
            cancleBtnClass: 'btn btn-default',
            confirmBtnClass: 'btn btn-default',
            color: '#5cb85c',
            header: 'Conferir Pedido',
            widthProsentage: 60,
            animation: 'fadeInUp'
        };
        this.popupEfetuarCompra.show(this.popupEfetuarCompra.options);
    }

    /*cancelar a compra no carrinho*/
    cancelarCompra() {
        if (confirm('Deseja cancelar a compra?')) {
            this.cartService.cleanCart();
            this.router.navigate(['home']);
        }
    }
  
    Confirm(){
        this.router.navigate(['payment']);
    }
    Cancel(){
        this.cartService.cleanCart();
        /*this.router.navigate(['cart']);*/
        this.popupEfetuarCompra.hide();
    }
}
