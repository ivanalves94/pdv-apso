import {Component, Input, OnInit} from '@angular/core';
import {CartItem} from '../../../models/cart-item';
import {CartService} from '../../../services/cart.service';

@Component({
  selector: '[app-confirm-cart]',
  templateUrl: './confirm-cart.component.html',
  styleUrls: ['./confirm-cart.component.css']
})
export class ConfirmCartComponent implements OnInit {
  @Input() cartItem: CartItem;
  
  constructor(private cartService: CartService) { }

  ngOnInit() {
  }

}
