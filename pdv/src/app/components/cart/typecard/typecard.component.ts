import {Component, OnInit, ViewChild} from '@angular/core';
import {CartService} from '../../../services/cart.service';
import {Cart} from '../../../models/cart';
import {Router} from '@angular/router';


import {Popup} from 'ng2-opd-popup';

@Component({
    selector: 'app-typecard',
    templateUrl: './typecard.component.html',
    styleUrls: ['./typecard.component.css']
})
export class TypecardComponent implements OnInit {

    cart: Cart;
    optionCard = false;

    @ViewChild('popupCredito') popupCredito: Popup;
    @ViewChild('popupDebito') popupDebito: Popup;
    @ViewChild('popupFinalizado') popupFinalizado: Popup;

    constructor(private cartService: CartService, private router: Router) {
    }

    // carrega o carrinho de compras ao iniciar o componente
    ngOnInit() {
        this.cart = this.cartService.getCart();
    }

    credito() {
        this.popupCredito.options = {
          confirmBtnContent: 'Ok',
          cancleBtnContent: 'Cancelar',
          cancleBtnClass: 'btn btn-default',
          confirmBtnClass: 'btn btn-default',
          color: '#5cb85c',
          header: 'Cartão de Crédito selecionado.',
          widthProsentage: 60,
          animation: 'fadeInUp'
        };
        this.popupCredito.show(this.popupCredito.options);
    }
    debito() {
        this.popupDebito.options = {
          confirmBtnContent: 'Ok',
          cancleBtnContent: 'Cancelar',
          cancleBtnClass: 'btn btn-default',
          confirmBtnClass: 'btn btn-default',
          color: '#5cb85c',
          header: 'Cartão de Débito selecionado.',
          widthProsentage: 60,
          animation: 'fadeInUp'
        };
        this.popupDebito.show(this.popupDebito.options);
    }
    finalizarCompra() {
        this.popupFinalizado.options = {
            confirmBtnContent: 'Ok',
            cancleBtnContent: 'Cancelar',
            cancleBtnClass: 'btn btn-default',
            confirmBtnClass: 'btn btn-default',
            color: '#5cb85c',
            header: 'Sucesso!',
            widthProsentage: 60,
            animation: 'fadeInUp'
        };
        this.popupFinalizado.show(this.popupFinalizado.options);
    }
    Confirm(){
        this.popupDebito.hide();
        this.popupCredito.hide();
        this.optionCard = true;
    }
    Cancel(){
        this.popupDebito.hide();
        this.popupCredito.hide();
        this.optionCard = false;
        /*this.popupEfetuarCompra.hide();*/
    }
    Voltar() {
      this.cartService.cleanCart();
      //this.router.navigate(['cart']);
      this.router.navigate(['search-client']);
    }
}
