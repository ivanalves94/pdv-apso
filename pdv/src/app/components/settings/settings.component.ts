import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private afAuth: AngularFireAuth, private router: Router) { 

  }

  nameUser: String;
  telephone: number;
  email: String;
  birth: String;
  address: String;
  admission: String;
  cpf: number;
  rg: number;


  ngOnInit() {
    this.nameUser = "Ivan Sousa ";
    this.telephone = 85987545890;
    this.birth = "01/02/1994";
    this.email = "admin@gmail.com";
    this.address = "Rua: 12, numero 15, Novo Maracanaú";
    this.admission = "02/05/2018"; 
    this.rg = 87897879879;
    this.cpf = 7879879879;

  }
   
  logout(){
    this.afAuth.auth.signOut();
    this.router.navigate(["/"]);
  }


}
