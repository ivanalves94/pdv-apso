import { Component, OnInit } from '@angular/core';
import { FormsModule, NgForm, FormGroup } from '@angular/forms'; import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  constructor(private router: Router, private database: AngularFireDatabase, private afAuth: AngularFireAuth) {


   }

  ngOnInit() {
  }

  registerForm(f: NgForm) {


    if (f.controls.password2.value == f.controls.password.value) {
      
      this.afAuth.auth.createUserWithEmailAndPassword(f.controls.email.value, f.controls.password.value).then(ok => {

        console.log(this.afAuth.auth.currentUser.uid)
       
        this.database.list("funcionarios/").set(this.afAuth.auth.currentUser.uid, {
          name: f.controls.name.value,
          email: f.controls.email.value,
          rg: f.controls.rg.value,
          cpf: f.controls.cpf.value,
          admission: f.controls.admission.value,
          birth: f.controls.birth.value,
          type: f.controls.typeUser.value,
          telephone: f.controls.telephone.value,
          address: f.controls.address.value,
          gender: f.controls.gender.value
        });

        console.log('User successfully registered!');

      }).catch((e) => {

        alert(e);

      });
    } else {
      alert("Senhas não são iguais.");
    }

  }


}
