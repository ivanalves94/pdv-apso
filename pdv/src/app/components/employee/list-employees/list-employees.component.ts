import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Employee} from '../../../models/employee';
import {EmployeeService} from '../../../services/employee.service';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  
  @Input() employee: Employee;
  @Output() removeEmployeeEvent = new EventEmitter();
  
  employees: Employee[];
  constructor(private employeeService: EmployeeService) { }
  
  ngOnInit() {
    this.employees = this.employeeService.getEmployees();
  }
  
  onRemoveEmployee(){
    this.employees = this.employeeService.getEmployees();
  }
}
