import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { EmployeeService } from '../../../services/employee.service';
import {Popup} from 'ng2-opd-popup';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.css'],
  providers: [EmployeeService]
})
export class NewEmployeeComponent implements OnInit {

  employeeForm: FormGroup;
  @ViewChild('popupNovoEmpregadoSucesso') popupNovoEmpregadoSucesso: Popup;

    constructor(public fb: FormBuilder, private employeeService: EmployeeService, private router: Router) {
        this.employeeForm = fb.group({
            'name': ['', Validators.required],
            'email': ['', [Validators.required, Validators.email]],
            'cpf': ['', Validators.required]
        });
    }

    onSubmit() {
        this.employeeService.saveEmployee(this.employeeForm.value);
        this.employeeService.getEmployees().forEach((employee) => console.log(employee));

        this.popupNovoEmpregadoSucesso.options = {
            confirmBtnContent: 'Confirmar',
            cancleBtnClass: 'btn btn-default',
            confirmBtnClass: 'btn btn-default',
            color: '#5cb85c',
            header: 'Sucesso',
            widthProsentage: 60,
            animation: 'bounceInDown'
        };
        this.popupNovoEmpregadoSucesso.show(this.popupNovoEmpregadoSucesso.options);
    }

    Confirm(){
        this.router.navigate(['employees']);
    }

    ngOnInit() {
    }

}
