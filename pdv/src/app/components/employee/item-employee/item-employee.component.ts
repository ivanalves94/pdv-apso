import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Employee} from '../../../models/employee';
import {EmployeeService} from '../../../services/employee.service';

@Component({
  selector: '[app-item-employee]',
  templateUrl: './item-employee.component.html',
  styleUrls: ['./item-employee.component.css']
})
export class ItemEmployeeComponent implements OnInit {
  
  
  @Input() employee: Employee;
  @Output() removeEmployeeEvent = new EventEmitter();
  
  
  /*constructor(private employeeService: EmployeeService) { }

  removeEmployee() {
    this.employeeService.removeEmployee(this.employee);
    this.removeEmployeeEvent.emit();
  }

  ngOnInit() {
  }*/
  
  employees: Employee[];
  
  constructor(private employeeService: EmployeeService) {
  }
  
  ngOnInit() {
    this.employees = this.employeeService.getEmployees();
  }
  
  // onRemoveEmployee() {
  //   this.employees = this.employeeService.getEmployees();
  // }
  
  removeEmployee() {
    this.employeeService.removeEmployee(this.employee);
    this.removeEmployeeEvent.emit();
  }
}
