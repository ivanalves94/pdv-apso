import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmployeeService} from '../../../services/employee.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Popup} from 'ng2-opd-popup';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

    employeeForm: FormGroup;
    @ViewChild('popupEditarEmpregadoSucesso') popupEditarEmpregadoSucesso: Popup;

    constructor(private fb: FormBuilder, private employeeService: EmployeeService, private router: Router, private activatedRoute: ActivatedRoute) {
      this.activatedRoute.params.forEach((params: Params) => {
        const id = params['id'];
        const employee = this.employeeService.getEmployeeById(+id);
        this.employeeForm = fb.group({
          'id': [employee.id],
          'name': [employee.name, Validators.required],
          'email': [employee.email, [Validators.required, Validators.email]],
          'cpf': [employee.cpf, Validators.required]
        });
      });
    }

    onSubmit() {
      this.employeeService.updateEmployee(this.employeeForm.value);

      this.popupEditarEmpregadoSucesso.options = {
        confirmBtnContent: 'Confirmar',
        cancleBtnClass: 'btn btn-default',
        confirmBtnClass: 'btn btn-default',
        color: '#5cb85c',
        header: 'Sucesso',
        widthProsentage: 60,
        animation: 'bounceInDown'
      };
      this.popupEditarEmpregadoSucesso.show(this.popupEditarEmpregadoSucesso.options);

    }

    Confirm(){
      this.router.navigate(['employees']);
    }

    ngOnInit() {
    }

}
