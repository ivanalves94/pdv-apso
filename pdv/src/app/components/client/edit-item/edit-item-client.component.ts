import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ClientService} from '../../../services/client.service';

@Component({
    selector: 'app-edit-item-client',
    templateUrl: './edit-item-client.component.html',
    styleUrls: ['./edit-item-client.component.css']
})
export class EditItemClientComponent implements OnInit {

    clientForm: FormGroup;

    constructor(private fb: FormBuilder, private clientService: ClientService, private router: Router, private activatedRoute: ActivatedRoute) {
        this.activatedRoute.params.forEach((params: Params) => {
            const id = params['id'];
            const client = this.clientService.getClientById(+id);
            this.clientForm = fb.group({
                'id': [client.id],
                'name': [client.name, Validators.required],
                'telephone': [client.telephone, Validators.required],
                'cpf': [client.cpf, Validators.required],
                'address': [client.address, Validators.required]
            });
        });
    }

    // atualiza o cliente
    onSubmit() {
        this.clientService.updateClient(this.clientForm.value);
        this.router.navigate(['/clients']);
    }

    ngOnInit() {
    }

}
