import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, ValidatorFn, AbstractControl, Validators} from '@angular/forms';
import {ClientService} from '../../../services/client.service';
import {Client} from '../../../models/client';
import {CPF} from 'cpf_cnpj';
import {CNPJ} from 'cpf_cnpj';
import {SearchService} from '../../../services/search.service';

@Component({
    selector: 'app-search-client',
    templateUrl: './search-client.component.html',
    styleUrls: ['./search-client.component.css']
})
export class SearchClientComponent implements OnInit {

    searchForm: FormGroup;

    clients: Client[];

    clientSearch: boolean;


    constructor(private fb: FormBuilder, private clientService: ClientService, private searchService: SearchService) {
        this.searchForm = fb.group({
            'name': [{value: '', disabled: true}],
            'telephone': [{value: '', disabled: true}],
            'cpf': ['', [Validators.required, this.validateCPFeCNPJ()] ],
            'address': [{value: '', disabled: true}],
        });
        this.search();
    }

    // carrega os clientes ao iniciar o componente
    ngOnInit() {
        this.clients = this.clientService.getClients();
    }

    // realiza a pesquisa por meio do cpf e cnpj
    search() {
        this.searchForm.get('cpf').valueChanges.subscribe((cpf) => {
            const clientSearch = this.clientService.getClientByCpf(cpf);

            this.searchService.setCPF(cpf);

            if (clientSearch) {
                this.clientSearch = true;
                this.searchForm.patchValue({name: clientSearch.name, telephone: clientSearch.telephone});
            } else {
                this.clientSearch = false;
                this.searchService.setCPF_NCadastrado(cpf);
            }
        });
    }
    // realiza a validação tanto pra CPF quanto para CNPJ
    validateCPFeCNPJ(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const invalid = CPF.isValid(control.value);
            const invalid2 = CNPJ.isValid(control.value);
            if (invalid) {
                return !invalid ? {valid: invalid} : null;

            }else if (invalid2) {
                return !invalid2 ? {valid: invalid2} : null;
            }else {
                return !invalid ? {valid: invalid} : null;
            }
        };
    }

}
