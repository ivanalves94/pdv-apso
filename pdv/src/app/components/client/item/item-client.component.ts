import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Client} from '../../../models/client';
import {ClientService} from '../../../services/client.service';

@Component({
    selector: '[app-item-client]',
    templateUrl: './item-client.component.html',
    styleUrls: ['./item-client.component.css']
})
export class ItemClientComponent implements OnInit {
    @Input() client: Client;
    @Output() removeClientEvent = new EventEmitter();

    constructor(private clientService: ClientService) {
    }

    removeClient() {
        this.clientService.removeClient(this.client);
        this.removeClientEvent.emit();
    }

    ngOnInit() {
    }

}
