import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import {ClientService} from '../../../services/client.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SearchClientComponent} from '../search-client/search-client.component';
import { CPF } from "cpf_cnpj";
import {SearchService} from '../../../services/search.service';

@Component({
    selector: 'app-new-client',
    templateUrl: './new-client.component.html',
    styleUrls: ['./new-client.component.css']
})

export class NewClientComponent implements OnInit {

    clientForm: FormGroup;
    path: string;
    cpf_value:SearchClientComponent;
    frase: string;

    constructor(private fb: FormBuilder, private clientService: ClientService, private router: Router, private activatedRoute: ActivatedRoute, private searchService: SearchService) {
        this.clientForm = fb.group({
            'name': ['', Validators.required],
            'telephone': ['', [Validators.required, Validators.required]],
            'cpf': ['', [Validators.required]], 
            'address': ['', Validators.required],
             });
        
    }

    // salva o cliente
    onSubmit() {
        this.clientService.saveClient(this.clientForm.value);
        this.clientService.getClients().forEach((client) => console.log(client));
        if (this.path) {
            this.router.navigate(['/cart']);
        } else {
            this.router.navigate(['/clients']);
        }
    }

    // pega o parametro passado pela rota
    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            this.path = params['vender'];
        });
        this.frase = this.searchService.getCPF_NCadastrado();
    }

    validateCPF(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const invalid = !CPF.isValid(control.value);
            return invalid ? {valid: invalid} : null;
        };
    }
}
