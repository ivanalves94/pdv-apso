import {Component, OnInit, Output} from '@angular/core';
import {Client} from '../../../models/client';
import {ClientService} from '../../../services/client.service';

@Component({
    selector: 'app-list-items-client',
    templateUrl: './list-items-client.component.html',
    styleUrls: ['./list-items-client.component.css']
})
export class ListItemsClientComponent implements OnInit {

    clients: Client[];

    constructor(private clientService: ClientService) {
    }

    // carrega os clientes ao iniciar o componente
    ngOnInit() {
        this.clients = this.clientService.getClients();
    }

    onRemoveClient() {
        this.clients = this.clientService.getClients();
    }
}
