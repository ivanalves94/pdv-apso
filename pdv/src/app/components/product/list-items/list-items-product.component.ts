import {Component, OnInit} from '@angular/core';
import {Product} from '../../../models/product';
import {ProductService} from '../../../services/product.service';

@Component({
    selector: 'app-list-items-product',
    templateUrl: './list-items-product.component.html',
    styleUrls: ['./list-items-product.component.css']
})
export class ListItemsProductComponent implements OnInit {

    products: Product[];

    constructor(private productService: ProductService) {
    }

    // carrega os produtos ao iniciar o componente
    ngOnInit() {
        this.products = this.productService.getProducts();
    }

    onRemoveProduct() {
        this.products = this.productService.getProducts();
    }

}
