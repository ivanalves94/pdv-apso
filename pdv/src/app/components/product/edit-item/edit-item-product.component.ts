import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../../services/product.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
    selector: 'app-edit-item-product',
    templateUrl: './edit-item-product.component.html',
    styleUrls: ['./edit-item-product.component.css']
})
export class EditItemProductComponent implements OnInit {

    productForm: FormGroup;

    constructor(private fb: FormBuilder, private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute) {
       
       
        this.activatedRoute.params.forEach((params: Params) => {
            const id = params['id'];
            const product = this.productService.getProductById(+id);
            this.productForm = fb.group({
                'id': [product.id],
                'name': [product.name, Validators.required],
                'description': [product.description, Validators.required],
                'price': [product.price, Validators.required]
            });
        });



        
    }

    // edita um produto
    onSubmit() {
        this.productService.updateProduct(this.productForm.value);
        alert('Produto atualizado com sucesso.')
        this.router.navigate(['/products']);
    }

    ngOnInit() {

    }

}
