import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../../../models/product';
import {ProductService} from '../../../services/product.service';

@Component({
    selector: '[app-item-product]',
    templateUrl: './item-product.component.html',
    styleUrls: ['./item-product.component.css']
})
export class ItemProductComponent implements OnInit {

    @Input() product: Product;
    @Output() removeProductEvent = new EventEmitter();

    constructor(private productService: ProductService) {
    }

    removeProduct() {
        this.productService.removeProduct(this.product);
        this.removeProductEvent.emit();
    }

    ngOnInit() {
    }

}
