import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../../services/product.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-new-product',
    templateUrl: './new-product.component.html',
    styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {


    productForm: FormGroup;

    constructor(private fb: FormBuilder, private productService: ProductService, private router: Router) {
        this.productForm = fb.group({
            'name': ['', Validators.required],
            'description': ['', Validators.required],
            'price': ['', Validators.required]
        });
    }

    // salva um produto no carrinho de compras
    onSubmit() {
        this.productService.saveProduct(this.productForm.value);
        this.productService.getProducts().forEach((product) => console.log(product));
        alert('Produto adicionado com sucesso.')
        this.router.navigate(['/products']);
    }

    ngOnInit() {

    }
}
