import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {LoginService} from '../../services/login.service';

@Component({
  selector: '[app-login]',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AngularFireAuth]
})
export class LoginComponent implements OnInit {


  


  constructor(private afAuth: AngularFireAuth, private router: Router, private loginService: LoginService) { }




  form_login(f: NgForm) {
    if (!f.valid) {
      return;
    }


    this.afAuth.auth.signInWithEmailAndPassword(f.controls.email.value, f.controls.password.value).then(ok => {
      this.loginService.setIdUser(this.afAuth);
      this.loginService.setEmailUser(this.afAuth);
      this.router.navigate(["/home"]);
    }).catch((error) => {
      alert(error.message);
    });
  }




  ngOnInit() {
  }

}
